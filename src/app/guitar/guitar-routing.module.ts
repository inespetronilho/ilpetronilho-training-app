import { StringsComponent } from "./strings/strings.component";
import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
// responsável por carregar os components dentro desta route.
// app.routing: faz /guitar (carrega este módulo)
//
const routes: Routes = [{ path: "", component: StringsComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GuitarRoutingModule {}
